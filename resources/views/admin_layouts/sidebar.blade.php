
        <div class="nav-left-sidebar sidebar-dark">
            <div class="menu-list">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <a class="d-xl-none d-lg-none d-md-none dashboard_label" href="#">Dashboard</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse nav_left_p" id="navbarNav">
                        <ul class="navbar-nav flex-column">
                            <li class="nav-divider">
                                <div class="avatar_menu">
                                <img src="{{ asset('assets/images/avator.png') }}" width="190" height="199" alt="">
                                
                                
                                </div>
                                <h3 class="avatar_welcome_label">Welcome</h3>
                                <label class="avatar_label">A.Atencio</label>
                                
                          </li>
                            <li class="nav-item ">
                                <a class="nav-link active" href="#" ><img src="{{ asset('assets/images/agent_manage_icon.png') }}" width="20" height="19" alt=""> Agents Manage	</a>
                                
                            </li>
                            
                               <li class="nav-item ">
                                <a class="nav-link" href="#" ><img src="{{ asset('assets/images/country_icon.png') }}" width="20" height="20" alt=""> Country State City Zip	</a>
                                
                            </li>
                            
                                  <li class="nav-item ">
                                <a class="nav-link" href="#" ><img src="{{ asset('assets/images/rule_icon.png') }}" width="18" height="20" alt=""> Rules & Tiers Manage	</a>
                                
                            </li>
                            
                            
                                      <li class="nav-item ">
                                <a class="nav-link" href="{{route('office.create')}}" ><img src="{{ asset('assets/images/office_manage_icon.png') }}" width="20" height="20" alt=""> Offices Manage	</a>
                                
                            </li>
                                  <li class="nav-item ">
                                <a class="nav-link" href="#" ><img src="{{ asset('assets/images/property.png') }}" width="20" height="20" alt=""> Property Manage	</a>
                                
                            </li>
                                  <li class="nav-item ">
                                <a class="nav-link" href="#" ><img src="{{ asset('assets/images/compani.png') }}" width="20" height="21" alt=""> Management Companies	</a>
                                
                            </li>
                                  <li class="nav-item ">
                                <a class="nav-link" href="#" ><img src="{{ asset('assets/images/auto_pay.png') }}" width="19" height="22" alt=""> Auto Pay Manage	</a>
                                
                            </li>
                              </li>
                                  <li class="nav-item ">
                                <a class="nav-link" href="#" ><img src="{{ asset('assets/images/manage_inovice.png') }}" width="20" height="21" alt=""> Manage Invoices	</a>
                                
                            </li>
                                    <li class="nav-item ">
                                <a class="nav-link" href="#" ><img src="{{ asset('assets/images/ad_inovice.png') }}" width="20" height="21" alt=""> Add Invoices	</a>
                                
                            </li>
                                   <li class="nav-item ">
                                <a class="nav-link" href="#" ><img src="{{ asset('assets/images/search_inovice.png') }}" width="20" height="20" alt=""> Search Invoices	</a>
                                
                            </li>
                                           <li class="nav-item ">
                                <a class="nav-link" href="#" ><img src="{{ asset('assets/images/search_payment.png') }}" width="25" height="20" alt=""> Search Payment	</a>
                                
                            </li>
                            
                                              <li class="nav-item ">
                                <a class="nav-link" href="#" ><img src="{{ asset('assets/images/check_manage.png') }}" width="22" height="22" alt=""> Checks Manage	</a>
                                
                            </li>
                            
                 <li class="nav-item ">
                                <a class="nav-link" href="#" ><img src="{{ asset('assets/images/payment_manage.png') }}" width="20" height="20" alt=""> Payment Manage</a>
                                
                            </li>
                                             
                 <li class="nav-item ">
                                <a class="nav-link" href="#" ><img src="{{ asset('assets/images/deposite.png') }}" width="22" height="24" alt=""> Deposit Manage</a>
                                
                            </li>
                            
                                 <li class="nav-item ">
                                <a class="nav-link" href="#" ><img src="{{ asset('assets/images/approval_icon.png') }}" width="20" height="22" alt=""> Approval Manage</a>
                                
                            </li>
    <li class="nav-item ">
                                <a class="nav-link" href="#" ><img src="{{ asset('assets/images/reports.png') }}" width="18" height="23" alt=""> Reports Manage</a>
                                
                            </li>
   </li>
    <li class="nav-item ">
                                <a class="nav-link" href="#" ><img src="{{ asset('assets/images/user_manage.png') }}" width="20" height="20" alt=""> Users Manage</a>
                                
                            </li>
                            
                               <li class="nav-item ">
                                <a class="nav-link" href="#" ><img src="{{ asset('assets/images/search_inovice.png') }}" width="20" height="20" alt=""> Forms Manage</a>
                                
                            </li>
 
     <li class="nav-item ">
                                <a class="nav-link" href="#" ><img src="{{ asset('assets/images/compani.png') }}" width="20" height="21" alt=""> Company Information Manage</a>
                                
                            </li>
 
     <li class="nav-item ">
                                <a class="nav-link" href="#" ><img src="{{ asset('assets/images/company_admin.png') }}" width="20" height="19" alt=""> Company Admins	 Manage</a>
                                
                            </li>

     <li class="nav-item ">
                                <a class="nav-link" href="#" ><img src="{{ asset('assets/images/search_inovice.png') }}" width="20" height="20" alt=""> Security Manage</a>
                                
                            </li>

     <li class="nav-item ">
                                <a class="nav-link" href="#" ><img src="{{ asset('assets/images/country_icon.png') }}" width="20" height="20" alt=""> Change Password</a>
                                
                            </li> 
                            
                        </ul>
                    </div>
                </nav>
            </div>
        </div>